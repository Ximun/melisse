{ config, pkgs, ... }:
let hostname = "malastare";
in {
  imports = [ # imports
    ../../../modules/backup/client.nix
    ../../../modules/monitoring/client.nix
    ../../common
    ../../common/secrets.nix
    ../../common/qemu-guest
    ../../common/qemu-guest/uefi.nix
  ];

  # Use the GRUB 2 boot loader.
  boot.kernel.sysctl = {
    "net.ipv4.ip_forward" = 1;
    "net.ipv6.conf.all.forwarding" = 1;
  };

  # Set your time zone.
  time.timeZone = "Europe/Paris";

  monitoring.client.enable = true;
  monitoring.client.host = hostname;

  # Set networking
  networking = {
    defaultGateway6 = {
      address = "2a0c:e300:12::190";
      interface = "ens3";
    };
    defaultGateway = {
      address = "185.233.102.190";
      interface = "ens3";
    };
    firewall.checkReversePath = false;
    hostName = "malastare";
    nameservers = [ "185.233.100.100" "185.233.100.101" "1.1.1.1" ];
    interfaces.ens3 = {
      mtu = 1378;
      ipv4 = {
        addresses = [{
          address = "185.233.102.134";
          prefixLength = 26;
        }];
      };
      ipv6 = {
        addresses = [{
          address = "2a0c:e300:12::134";
          prefixLength = 48;
        }];
      };
    };
    interfaces.ens4 = {
      ipv6 = {
        addresses = [
          {
            address = "2a0c:e304:c0fe::1";
            prefixLength = 64;
          }
          {
            address = "2a0c:e304:c0fe:1::1";
            prefixLength = 64;
          }
        ];
      };
    };
    interfaces.ens10 = {
      ipv4 = {
        addresses = [{
          address = "10.1.0.1";
          prefixLength = 16;
        }];
      };
    };
  };

  backup.client.enable = true;
  backup.client.host = hostname;
  backup.client.paths = [ "/var/log" ];

  # DHCPv4
  networking.firewall.interfaces.ens10.allowedUDPPorts = [ 67 ];
  networking.nat = {
    enable = true;
    internalIPs = [ "10.1.0.0/16" ];
    internalInterfaces = [ "ens5" ];
    externalIP = "185.233.102.134";
    externalInterface = "ens3";
  };
  services.dhcpd4 = {
    enable = true;
    interfaces = [ "ens10" ];
    extraConfig = ''
      option subnet-mask 255.255.0.0;
      option broadcast-address 10.1.255.255;
      option routers 10.1.0.1;
      option domain-name-servers 185.233.100.100;
      option domain-name "melisse.org";
      subnet 10.1.0.0 netmask 255.255.0.0 {
        range 10.1.0.10 10.1.1.250;
      }
    '';
  };

  # DHCPv6
  networking.firewall.interfaces.ens4.allowedUDPPorts = [ 547 ];
  systemd.services.dhcpd6.serviceConfig.AmbientCapabilities = [ "CAP_NET_RAW" ];
  services.dhcpd6 = {
    enable = true;
    interfaces = [ "ens4" ];
    extraConfig = ''
      option dhcp6.name-servers 2a0c:e300::100;
      subnet6 2a0c:e304:c0fe:1::/64 {
        range6 2a0c:e304:c0fe:1::D:1 2a0c:e304:c0fe:1::D:FFFF;
      }
    '';
  };
  services.radvd = {
    enable = true;
    config = ''
      interface ens4 {
        AdvSendAdvert on;
        AdvManagedFlag on;
        prefix 2a0c:e304:c0fe:1::/64 {
          AdvRouterAddr on;
        };
      };
    '';
  };

  age.secrets = {
    backup_passwd = { file = ../../../secrets/malastare_backup_passwd.age; };
  };

  system.stateVersion = "20.09";
}
